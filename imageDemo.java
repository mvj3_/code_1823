String imagePath = Environment.getExternalStorageDirectory()
							.getAbsolutePath() + "/xxx.jpg";
File imageFile = new File(imagePath);
Uri imageUri = Uri.fromFile( imageFile );

Intent intent = new Intent( android.provider.MediaStore.ACTION_IMAGE_CAPTURE );
intent.putExtra ( android.provider.MediaStore.EXTRA_OUTPUT , imageUri );
startActivitForResult( intent, 1 );

//MediaStore类中的EXTRA_OUTPUT常量可以将以 Uri的方式指示 Camera把图像保存在什么地方。


//BitmapFactory.Options 中指定 inSampleSize 可以指明加载时 Bitmap 图像的比例。
//BitmapFactory.Options.inJustDecodeBounds = true 时，将通知 BitmapFactory类
//只须返回该图像的范围，而无须尝试解码图像本身。

Display display =getWindowManager().getDefaultDisplay();
int dw = display.getWidth();
int dh = display.getHeght();
Options options = new Options();
options.inJustDecodeBounds = true;
Bitmap bmp =BitmapFactory.decodeFile( xxx ,options );
int heightRatio = (int)Math.ceil( options.outHeight / (float)dh );
int widthRatio = (int)Math.ceil( options.outWidth / (float)dw );

//如果两边的比例都大于1，那么一条边将大于屏幕
if( heightRatio >1 && widthRatio >1)
	if( heightRatio > widthRatio )
		options.inSampleSize = heightRatio;
	else
		options.inSampleSize = widthRatio;
options.inJustDecodeBounds = false;
bmp = BitmapFactory.decodeFile(xxx , options); 